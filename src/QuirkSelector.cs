﻿using BepInEx;
using HarmonyLib;
using QuirkSelector.Hooks;

namespace QuirkSelector
{
    [BepInPlugin("dev.isekai.mods.slipways.quirk_selector", "Quirk Selector", "0.6.0")]
    internal partial class QuirkSelector : BaseUnityPlugin
    {
        private static QuirkSelectorConfig? _conf;
        internal static QuirkSelectorConfig Conf => _conf ??= new(FindObjectOfType<QuirkSelector>());

        private void Awake()
        {
            Harmony.CreateAndPatchAll(typeof(QuirkSelectorHook));
            Harmony.CreateAndPatchAll(typeof(SectorMapGuiHook));
            Harmony.CreateAndPatchAll(typeof(SectorMapRandomisationHook));
        }
    }
}
