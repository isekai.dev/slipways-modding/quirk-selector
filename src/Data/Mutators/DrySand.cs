using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record DrySand : Mutator
    {
        public override string LabelId => "mutator.dry_sand";

        public override string Label => "Dry sand";

        public override string DescriptionId => "mutator.more_planets_of_two_types.desc";

        public override object[] DescriptionParams => new object[] {"planet.barren", "planet.arid"};


        public override string Description =>
            "This sector has an altered composition, with significantly more [[ref:{1}]] and [[ref:{2}]] planets.";

        public override MutatorKind Kind => MutatorKind.Composition;

        public override bool Hidden => false;

        public override int? ScoreModifier => 10;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MapGenGuaranteeHot()"};

        public override List<string>? Effects => null;
    }
}
