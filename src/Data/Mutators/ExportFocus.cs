using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record ExportFocus : Mutator
    {
        public override string LabelId => "mutator.score_for_exports";

        public override string Label => "Export focus";

        public override string DescriptionId => "mutator.scoring_replacement.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("scoring.mut.exports.desc",
                "Each planet with at least {1} exports is worth *{2}:star:*.",
                4, 350
            )
        };


        public override string Description => "Replaces the scoring for empire size with the following rule:\n{1}";

        public override MutatorKind Kind => MutatorKind.ScoreReplacing;

        public override bool Hidden => false;

        public override int? ScoreModifier => null;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MutReplaceEmpireSizeScoring(MutScoringExports(4, 350))"};

        public override List<string>? Effects => null;
    }
}
