using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record ProjectFocus : Mutator
    {
        public override string LabelId => "mutator.score_for_projects";

        public override string Label => "Project focus";

        public override string DescriptionId => "mutator.scoring_replacement.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("scoring.mut.projects.desc",
                "Each planet with a planetary project is worth *{1}:star:*.",
                150
            )
        };


        public override string Description => "Replaces the scoring for empire size with the following rule:\n{1}";

        public override MutatorKind Kind => MutatorKind.ScoreReplacing;

        public override bool Hidden => false;

        public override int? ScoreModifier => null;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MutReplaceEmpireSizeScoring(MutScoringProjects(150))"};

        public override List<string>? Effects => null;
    }
}
