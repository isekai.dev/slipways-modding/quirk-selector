using System.Collections.Generic;
using Slipways.General.Localizations;

namespace QuirkSelector.Data.Mutators
{
    public record TechnologyFocus : Mutator
    {
        public override string LabelId => "mutator.score_for_techs";

        public override string Label => "Technology focus";

        public override string DescriptionId => "mutator.scoring_replacement.desc";

        public override object[] DescriptionParams => new object[] {
            new LocalizedString("scoring.mut.techs.desc",
                "Each invented technology is worth *{1}:star:*.",
                300
            )
        };


        public override string Description => "Replaces the scoring for empire size with the following rule:\n{1}";

        public override MutatorKind Kind => MutatorKind.ScoreReplacing;

        public override bool Hidden => false;

        public override int? ScoreModifier => null;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MutReplaceEmpireSizeScoring(MutScoringTechs(300))"};

        public override List<string>? Effects => null;
    }
}
