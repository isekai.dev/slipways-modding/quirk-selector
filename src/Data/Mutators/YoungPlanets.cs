using System.Collections.Generic;

namespace QuirkSelector.Data.Mutators
{
    public record YoungPlanets : Mutator
    {
        public override string LabelId => "mutator.young";

        public override string Label => "Young planets";

        public override string DescriptionId => "mutator.more_planets_of_two_types.desc";

        public override object[] DescriptionParams => new object[] {"planet.primordial", "planet.lava"};


        public override string Description =>
            "This sector has an altered composition, with significantly more [[ref:{1}]] and [[ref:{2}]] planets.";

        public override MutatorKind Kind => MutatorKind.Composition;

        public override bool Hidden => false;

        public override int? ScoreModifier => 0;

        public override List<string>? Packages => null;

        public override List<string> Conditions => new() {"MapGenGuaranteeYoung()"};

        public override List<string>? Effects => null;
    }
}
